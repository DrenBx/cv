function ft_comp
{
	for schr in $(find . -type d -iname '.git')
	do
		mv -f $schr ${schr}_
	done
	mv -f .git_/ .git/
}

function ft_rm_DS_Store
{
	for schr in $(find . -type d -iname '.DS_Store')
	do
		rm -Rf $schr
	done
	rm -Rf .DS_Store
}

function ft_decomp
{
	for schr in $(find . -type d -iname '.git_')
	do
		mv -f $schr `echo $schr | sed 's/\.git_/\.git/'`
	done
}

if [ "$1" = "init" ]
then
	if [ "$2" != "" ]
	then
		echo "Init!"
		git init
		git remote add origin $2
	else
		echo "Usage: $0 init <git_URL>"
	fi
elif [ "$1" = "push" ]
then
	echo "Push!"
	ft_rm_DS_Store
	echo "Cleaning done!"
	ft_comp
	echo "Comp done!"
	git add -Af
	git commit -am "Auto update - $(date)"
	git push origin master
	echo "Push done!"
	ft_decomp
	echo "Decomp done!"
elif [ "$1" = "pull" ]
then
	echo "Pull!"
	git pull
	echo "Pull done!"
	ft_decomp
	echo "Decomp done!"
else
	echo "Usage:"
	echo "$0 init <git_URL>"
	echo "$0 pull"
	echo "$0 push"
fi

